import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: "http://localhost:3001/articulos/"
  });

const articuloService = {

    obtenerTodos() {
        return axiosInstance.get()
    },

    obtener(idArticulo) {
        return axiosInstance.get(idArticulo)
    }

}

export default articuloService
import Vue from "vue";
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Articulo from './views/Articulo.vue'
import CrearArticulo from './views/CrearArticulo.vue'
    
Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/articulo/:id",
    name: "articulo",
    component: Articulo
  },
  {
    path: "/crear-articulo",
    name: "crearArticulo",
    component: CrearArticulo
  }
];

const router = new VueRouter({
    mode: "history",
    routes
  });
  
export default router;